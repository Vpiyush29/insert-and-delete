package com.piyush.fifosqlite

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.collections.ArrayList
import java.util.Queue as Queue1

class DBHelper(context: Context): SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VER) {
//    internal lateinit var lPA : ListPersonAdapter
    internal var lstPersons:List<Person> = ArrayList<Person>()

    companion object {
        private  val  DATABASE_VER = 1
        private val  DATABASE_NAME="FIFO.db"

        private  val TABLE_NAME = "Person"
        private val COL_ID = "Id"
        private val COL_NAME ="Name"
        private val COL_EMAIL ="Email"

    }



    override fun onCreate(db: SQLiteDatabase?) {
        val CREATE_TABLE_QUERY = ("CREATE TABLE $TABLE_NAME ($COL_ID INTEGER PRIMARY KEY, $COL_NAME TEXT,$COL_EMAIL TEXT)")
        db!!.execSQL(CREATE_TABLE_QUERY);
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(db!!)
    }

    //CRUD

    val allPerson:List<Person>

        get() {

            val lstPersons = ArrayList<Person>()

            val selectQuery = "SELECT * FROM $TABLE_NAME"

            val db = this.writableDatabase

            val cursor = db.rawQuery(selectQuery, null)

            if (cursor.moveToFirst()) {
                do {
                    val person= Person()
                    person.id = cursor.getInt(cursor.getColumnIndex(COL_ID))
                    person.name = cursor.getString(cursor.getColumnIndex(COL_NAME))
                    person.email = cursor.getString(cursor.getColumnIndex(COL_EMAIL))

                    lstPersons.add(person)

                } while (cursor.moveToNext())
            }
            db.close()

            return lstPersons

        }



    fun addPerson(person: Person){
        val db = this.writableDatabase
//        lateinit var lsa : ListPersonAdapter

//        if (lstPersons.size > 2) {
//            val allDetails: List<Person> = allPerson
//            val oldestDetails: Person = allDetails[allDetails.size - 1]
//            deletePerson(oldestDetails)
//        }

            val values = ContentValues()
            values.put(COL_ID, person.id)
            values.put(COL_NAME, person.name)
            values.put(COL_EMAIL, person.email)

            db.insert(TABLE_NAME, null, values)
            db.close()

    }

    fun deletePerson(person: Person){
        val db= this.readableDatabase
//        db.delete(TABLE_NAME, "$COL_ID=?", arrayOf(person.id.toString()))
        db.delete(TABLE_NAME, "$COL_ID=?", arrayOf(person.id.
        toString()))
        db.close()
//        val allDetails: List<Person> = allPerson
//        val oldestDetails: Person = allDetails[allDetails.size - 1]
//
//        db.delete(TABLE_NAME, "$oldestDetails=?", arrayOf(person.id.toString()))
////        db.delete(TABLE_NAME, "$COL_ID=?", arrayOf(person.id.toString()))
//        db.close()
    }
}

