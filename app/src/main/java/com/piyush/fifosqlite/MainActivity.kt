package com.piyush.fifosqlite

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    internal lateinit var db: DBHelper
    internal var lstPersons:List<Person> = ArrayList<Person>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        db = DBHelper(this)

        refreshData()

        btnAdd.setOnClickListener{
            val person = Person(
                Integer.parseInt(etId.text.toString()),
                etName.text.toString(),
                etEmail.text.toString()
            )
            db.addPerson(person)
            refreshData()
        }

        btnDelete.setOnClickListener{
            val person = Person(
                Integer.parseInt(etId.text.toString()),
                etName.text.toString(),
                etEmail.text.toString()
            )
            db.deletePerson(person)
            refreshData()
        }
    }
    private fun refreshData(){
        lstPersons = db.allPerson

        val adapter = ListPersonAdapter(this@MainActivity,lstPersons,etId, etName, etEmail)
        list_person.adapter = adapter
    }
}